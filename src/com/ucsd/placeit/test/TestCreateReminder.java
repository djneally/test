
package com.ucsd.placeit.test;

import android.os.RemoteException;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;

public class TestCreateReminder extends UiAutomatorTestCase {

	public void testCreateRem() throws UiObjectNotFoundException, RemoteException {
	
		//values used when clicking the map
		int x = 500;
		int y = 500;
		int step = 20;
	
		//creates the UI device object to use for User Inputs on screen
		UiDevice device = getUiDevice();
		//device.pressHome();
		//device.pressRecentApps(); in case not already in PlaceIt app will find in recents
	
		//object for placeit app made to find in recents menu
		UiObject placeIt = new UiObject(new UiSelector() 
			.className("android.widget.TextView")
			.text("Place it"));
			
	
		placeIt.clickAndWaitForNewWindow(); //open placeIt
		
		UiObject map = new UiObject(new UiSelector()
			.className("android.widget.RelativeLayout")
			.index(0));
		
		UiObject zoomOut = new UiObject(new UiSelector()
			.className("android.view.View")
			.index(1));
		
		/*
		 * 
		 * Given: That the user is on the map view
		 * When:  User drags the map using the touch screen
		 * Then:  The map will move in the opposite direction of the finger movement
		 */
		assertTrue("did not swipe map", map.swipeUp(step));
		assertTrue("did not swipe map", map.swipeRight(step));
		assertTrue("did not swipe map", map.swipeDown(step));
		assertTrue("did not swipe map", map.swipeLeft(step));
		
		/*
		 * Given: That user is on the map view
		 * When:  User pinches on the screen
		 * Then:  The map will zoom out
		 */
		//map.pinchIn(10, step);
		
		/*
		 * Given: That user is on the map view
		 * When:  Expands fingers on the screen
		 * Then:   The map will zoom in
		 */
		//map.pinchOut(10, step);
	
		
		/*
		 * Given: The user is in map view
		 * When:  A user clicks on map
		 * And there is no reminder in that place clicked
		 * Then: A create new reminder menu pops up
		 * When: User fills in all fields with valid values
		 * And the user clicks submit
		 * Then: New reminder will be posted on the map in the location
		 * And the map will be focused with the reminder’s location in the center
		 */
		//once placeIts opens click where specified earlier (can be anywhere)
		//on click should create a placeit object on map
		device.click(x, y); 
	
		//newly placed placeit reminder. this object used to check that the 
		//onClick methods actually creates placeit on UIdevice
		UiObject enterText = new UiObject(new UiSelector()
			.text("task name..")
			.className("android.widget.EditText"));
	
		//assertTrue if after waiting 1 second there is a placeit object on screen
		enterText.setText("Blah");
		
		UiObject text2 = new UiObject(new UiSelector()
			.className("android.widget.EditText")
			.index(0));
		
		text2.click();
		text2.setText("bleep");
		
		UiObject subButton = new UiObject(new UiSelector()
			.className("android.widget.Button")
			.text("Submit"));
		
		subButton.clickAndWaitForNewWindow();
		
		assertTrue("not returned to map", map.exists());
		//end create reminder SBT
		
		device.click(x,y);
		
		subButton.click();
		
		UiObject okButton = new UiObject(new UiSelector()
			.className("android.widget.Button")
			.text("OK"));
		
		assertTrue("No error for invalid fields", okButton.exists());
		
		okButton.click();
		
		UiObject canButton = new UiObject(new UiSelector()
		.className("android.widget.Button")
		.text("Cancel"));
		
		canButton.click();
		assertTrue("Cancel button fail", map.exists());
		
		device.click(x,y);
		
		enterText.setText("Cheech & chong");
		text2.click();
		text2.setText("Light 'em up");
		subButton.click();
		
		UiObject delete = new UiObject(new UiSelector()
			.className("android.widget.Button")
			.index(0));
		
		UiObject later = new UiObject(new UiSelector()
			.className("android.widget.Button")
			.index(1));
		
		UiObject complete = new UiObject(new UiSelector()
			.className("android.widget.Button")
			.index(2));
		
		later.click();
		
		device.click(x, (y-50));
		device.waitForIdle(3000);
		device.click(385, y);
		
		complete.click();
	}
	
}
